import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';

import { Category } from '../../models/Category';
import { Condition } from '../../models/Condition';

import { CategoryService } from '../../services/category.service';

@Component({
  selector: 'app-blog-search-form',
  templateUrl: './blog-search-form.component.html',
  styleUrls: ['./blog-search-form.component.css']
})
export class BlogSearchFormComponent implements OnInit, OnDestroy {
  subscribeCategoryList: Subscription;
  categoryList: { label: string, value: number }[];
  id: number;
  title: string;
  desc: string;
  detail: string;
  categoryId: number;
  publicStatus: number = 0;
  condition: Condition = {};
  @Output() search = new EventEmitter<any>();

  constructor(
    private categoryService: CategoryService
  ) { }

  ngOnInit() {
    this.getCategoryList();
  }

  getCategoryList(): void {
    this.subscribeCategoryList = this.categoryService
      .getCategoryList()
      .subscribe((data: Category[]) => {
        if (Array.isArray(data) && data.length) {
          this.categoryList = data.map((item: Category) => {
            return { label: item.name, value: item.id }
          });
        }
      });
  }

  onSubmit(): void {
    this.id ? (this.condition.id = +this.id) : (delete this.condition.id);
    this.title ? (this.condition.title = this.title) : (delete this.condition.title);
    this.desc ? (this.condition.desc = this.desc) : (delete this.condition.desc);
    this.detail ? (this.condition.detail = this.detail) : (delete this.condition.detail);

    this.categoryId
      ? (this.condition.categoryId = +this.categoryId)
      : (delete this.condition.categoryId);

    this.publicStatus
      ? (this.condition.publicStatus = this.publicStatus === 1 ? true : false)
      : (delete this.condition.publicStatus);

      this.search.emit(this.condition);
  }

  clear() :void {
    this.id = null;
    this.title = null;
    this.desc = null;
    this.detail = null;
    this.categoryId = null;
    this.publicStatus = 0;
    this.condition = {};
    this.search.emit(this.condition);
  }

  ngOnDestroy() {
    if (this.subscribeCategoryList) {
      this.subscribeCategoryList.unsubscribe();
    }
  }
}
