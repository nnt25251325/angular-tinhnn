import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Blog } from '../../models/Blog';
import { Position } from '../../models/Position';
import { Condition } from '../../models/Condition';

import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { PositionService } from '../../services/position.service';
import { BlogService } from '../../services/blog.service';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit, OnDestroy {
  subscribeGetPositionList: Subscription;
  subscribeGetBlogList: Subscription;
  subscribeDeteleBlog: Subscription;
  positionList: Position[];
  blogList: Blog[];
  condition: Condition = {};

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private positionService: PositionService,
    private blogService: BlogService
  ) { }

  ngOnInit() {
    this.fetch();
  }

  fetch(): void {
    if (Array.isArray(this.positionList) && this.positionList.length) {
      this.getBlogList();
      return;
    }

    this.subscribeGetPositionList = this.positionService
      .getPositionList()
      .subscribe((data: Position[]) => {
        if (Array.isArray(data)) {
          this.positionList = data;
          this.getBlogList();
        }
      });
  }

  getBlogList(): void {
    this.subscribeGetBlogList = this.blogService
      .getBlogList(this.condition)
      .subscribe((data: Blog[]) => {
        if (Array.isArray(data)) {
          this.blogList = data.map((item: Blog) => {
            item.positionNameList =
              this.getPositionNameList(item.positionIds, this.positionList);
            return item;
          });
        }
      });
  }

  getPositionNameList(positionIds: number[], positionList: Position[]): string[] {
    const positionNameList = [];

    positionIds.forEach(positionId => {
      const found = positionList.find((item: Position) => item.id === +positionId);
      if (found && found.name) {
        positionNameList.push(found);
      }
    });

    return Array.isArray(positionNameList) && positionNameList.length
      ? positionNameList.map(item => item.name)
      : [];
  }

  onDelete(blogId: number): void {
    this.confirmationService.confirm({
      header: 'Delete Confirmation',
      message: 'Do you want to delete this blog?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.subscribeDeteleBlog = this.blogService
          .deteleBlog(blogId)
          .subscribe(() => {
            this.blogList = this.blogList.filter(item => item.id !== blogId);
            this.messageService.add({
              severity: 'success',
              detail: 'The blog was deleted successfully!'
            });
          });
      }
    });
  }

  onSearch(condition: Condition): void {
    this.condition = Object.assign({}, condition);
    this.fetch();
  }

  ngOnDestroy() {
    if (this.subscribeGetPositionList) {
      this.subscribeGetPositionList.unsubscribe();
    }
    if (this.subscribeGetBlogList) {
      this.subscribeGetBlogList.unsubscribe();
    }
    if (this.subscribeDeteleBlog) {
      this.subscribeDeteleBlog.unsubscribe();
    }
  }
}
