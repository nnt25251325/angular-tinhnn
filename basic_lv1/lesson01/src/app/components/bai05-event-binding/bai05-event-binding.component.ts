import { Component, OnInit } from '@angular/core';

import { Movie } from '../../models/Movie';
import { fakeMovies } from '../../fake-movies';

@Component({
  selector: 'app-bai05-event-binding',
  templateUrl: './bai05-event-binding.component.html',
  styleUrls: ['./bai05-event-binding.component.css']
})
export class Bai05EventBindingComponent implements OnInit {
  movies: Movie[] = fakeMovies;
  selectedMovie: Movie;

  constructor() { }

  ngOnInit() {
  }

  onSelect(movie: Movie): void {
    this.selectedMovie = movie;
    alert(`selectedMovie = ${JSON.stringify(this.selectedMovie)}`);
  }

}
