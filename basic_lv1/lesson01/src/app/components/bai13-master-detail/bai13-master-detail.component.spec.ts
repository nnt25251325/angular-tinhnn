import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai13MasterDetailComponent } from './bai13-master-detail.component';

describe('Bai13MasterDetailComponent', () => {
  let component: Bai13MasterDetailComponent;
  let fixture: ComponentFixture<Bai13MasterDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai13MasterDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai13MasterDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
