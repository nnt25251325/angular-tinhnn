import { Component, OnInit } from '@angular/core';

import { Movie } from '../../models/Movie';

@Component({
  selector: 'app-bai02-component',
  templateUrl: './bai02-component.component.html',
  styleUrls: ['./bai02-component.component.css']
})
export class Bai02ComponentComponent implements OnInit {
  movie: Movie = {
    id: 1,
    name: 'Star',
    releaseYear: 1988
  };

  constructor() { }

  ngOnInit() {
  }

}
