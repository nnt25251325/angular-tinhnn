import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai02ComponentComponent } from './bai02-component.component';

describe('Bai02ComponentComponent', () => {
  let component: Bai02ComponentComponent;
  let fixture: ComponentFixture<Bai02ComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai02ComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai02ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
