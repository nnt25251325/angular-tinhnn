import { Component, OnInit } from '@angular/core';

import { Movie } from '../../models/Movie';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'app-bai16-http-post',
  templateUrl: './bai16-http-post.component.html',
  styleUrls: ['./bai16-http-post.component.css']
})
export class Bai16HttpPostComponent implements OnInit {
  movies: Movie[];
  selectedMovie: Movie;

  constructor(
    private movieService: MovieService
  ) { }

  ngOnInit() {
    this.getMovies();
  }

  getMovies(): void {
    this.movieService.getMovies().subscribe((data: Movie[]) => this.movies = data);
  }

  add(name: string, releaseYear: string): void {
    name = name.trim();

    if (!name || Number.isNaN(+releaseYear) || +releaseYear === 0) {
      alert('Name must not be blank, Release year must be a number!');
      return;
    }

    const newMovie: Movie = new Movie();
    newMovie.name = name;
    newMovie.releaseYear = +releaseYear;
    console.log('METHOD: add', newMovie);

    this.movieService
      .addMovie(newMovie)
      .subscribe(item => this.movies.push(item));
  }
}
