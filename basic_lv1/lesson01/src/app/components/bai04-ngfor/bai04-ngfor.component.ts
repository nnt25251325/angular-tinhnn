import { Component, OnInit } from '@angular/core';

import { Movie } from '../../models/Movie';
import { fakeMovies } from '../../fake-movies';

@Component({
  selector: 'app-bai04-ngfor',
  templateUrl: './bai04-ngfor.component.html',
  styleUrls: ['./bai04-ngfor.component.css']
})
export class Bai04NgforComponent implements OnInit {
  movies: Movie[] = fakeMovies;

  constructor() { }

  ngOnInit() {
  }

}
