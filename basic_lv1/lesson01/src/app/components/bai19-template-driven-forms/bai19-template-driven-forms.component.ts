import { Component, OnInit } from '@angular/core';

import { Employee } from '../../models/Employee';

@Component({
  selector: 'app-bai19-template-driven-forms',
  templateUrl: './bai19-template-driven-forms.component.html',
  styleUrls: ['./bai19-template-driven-forms.component.css']
})
export class Bai19TemplateDrivenFormsComponent implements OnInit {
  jobCategories = ['technology', 'social', 'sciences', 'doctor'];
  newEmployee = new Employee(1, 'Hoang', new Date('2019/12/01'), this.jobCategories[0]);
  submitted = false;

  constructor() { }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
  }

  addNewEmployee() {
    this.newEmployee = new Employee(Math.floor(Date.now()), '', new Date(), '', '');
  }

}
