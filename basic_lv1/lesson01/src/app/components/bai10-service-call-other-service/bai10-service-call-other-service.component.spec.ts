import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai10ServiceCallOtherServiceComponent } from './bai10-service-call-other-service.component';

describe('Bai10ServiceCallOtherServiceComponent', () => {
  let component: Bai10ServiceCallOtherServiceComponent;
  let fixture: ComponentFixture<Bai10ServiceCallOtherServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai10ServiceCallOtherServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai10ServiceCallOtherServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
