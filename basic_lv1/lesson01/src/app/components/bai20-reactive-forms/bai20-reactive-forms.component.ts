import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User, Address, states } from '../../models/DataModel';
import { emailValidator } from '../../../shared/CustomValidators';

@Component({
  selector: 'app-bai20-reactive-forms',
  templateUrl: './bai20-reactive-forms.component.html',
  styleUrls: ['./bai20-reactive-forms.component.css']
})
export class Bai20ReactiveFormsComponent implements OnInit {
  states = states;
  userFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.userFormGroup = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', [Validators.required, emailValidator()]],
      addresses: this.formBuilder.group({
        street: ['', [Validators.required]],
        city: '',
        state: this.states[0],
      }),
    });
  }

  logUserFormGroup(): void {
    console.log(this.userFormGroup);
  }
}
