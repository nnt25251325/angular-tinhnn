import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai20ReactiveFormsComponent } from './bai20-reactive-forms.component';

describe('Bai20ReactiveFormsComponent', () => {
  let component: Bai20ReactiveFormsComponent;
  let fixture: ComponentFixture<Bai20ReactiveFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai20ReactiveFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai20ReactiveFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
