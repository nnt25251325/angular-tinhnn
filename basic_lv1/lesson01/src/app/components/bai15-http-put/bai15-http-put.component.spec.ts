import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai15HttpPutComponent } from './bai15-http-put.component';

describe('Bai15HttpPutComponent', () => {
  let component: Bai15HttpPutComponent;
  let fixture: ComponentFixture<Bai15HttpPutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai15HttpPutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai15HttpPutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
