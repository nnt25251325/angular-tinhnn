import { Component, OnInit } from '@angular/core';

import { Movie } from '../../models/Movie';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'app-bai15-http-put',
  templateUrl: './bai15-http-put.component.html',
  styleUrls: ['./bai15-http-put.component.css']
})
export class Bai15HttpPutComponent implements OnInit {
  movies: Movie[];
  selectedMovie: Movie;

  constructor(
    private movieService: MovieService
  ) { }

  ngOnInit() {
    this.getMovies();
  }

  getMovies(): void {
    this.movieService.getMovies().subscribe((data: Movie[]) => this.movies = data);
  }
}
