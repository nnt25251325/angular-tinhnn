import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai14HttpServicesComponent } from './bai14-http-services.component';

describe('Bai14HttpServicesComponent', () => {
  let component: Bai14HttpServicesComponent;
  let fixture: ComponentFixture<Bai14HttpServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai14HttpServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai14HttpServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
