import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai18SearchComponent } from './bai18-search.component';

describe('Bai18SearchComponent', () => {
  let component: Bai18SearchComponent;
  let fixture: ComponentFixture<Bai18SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai18SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai18SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
