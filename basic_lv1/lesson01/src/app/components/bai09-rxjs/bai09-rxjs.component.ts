import { Component, OnInit } from '@angular/core';

import { Movie } from '../../models/Movie';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'app-bai09-rxjs',
  templateUrl: './bai09-rxjs.component.html',
  styleUrls: ['./bai09-rxjs.component.css']
})
export class Bai09RxjsComponent implements OnInit {
  movies: Movie[];
  selectedMovie: Movie;

  constructor(
    private movieService: MovieService
  ) { }

  ngOnInit() {
    this.getMoviesFromServices();
  }

  getMoviesFromServices(): void {
    this.movieService.getMovies().subscribe((data: Movie[]) => this.movies = data);
  }

  onSelect(movie: Movie): void {
    this.selectedMovie = movie;
    console.log(`selectedMovie = ${JSON.stringify(this.selectedMovie)}`);
  }

}
